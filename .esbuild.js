import fs from 'fs'
import esbuild from 'esbuild'
import process from 'node:process'

const build = async (config) => {
  const { outdir } = config;

  try {
    if (!fs.existsSync(outdir)) {
      fs.mkdir(outdir, (err) => {
        if (err) throw err;
          console.log(`${outdir} created.`);
        });
    }

    esbuild
     .build(config).then(() => {
        fs.copyFile(
          './public/index.html',
          `${outdir}index.html`,
          (err) => {
            if (err) throw err;
          }
        );
      });
  } catch (err) {
    console.error(err)
  }
}

const server = async (config) => { 
  const es = await esbuild.context(config)

  await es.watch()
  await es.serve({
    servedir: 'public/',
    port: 9090
  })
}

const run = async (es) => {
  const CONFIG = {
    entryPoints: ['./src/index.tsx'],
    bundle: true
  }

  if (process.argv.includes('--serve')) {
    const DEV_CONFIG = {
      ...CONFIG,
      outdir: './public/',
      logLevel: 'info',
      write: true
    }

    server(DEV_CONFIG)

    return
  }

  const PROD_CONFIG = {
    ...CONFIG,
    outdir: './dist/',
    minify: true
  }

  build(PROD_CONFIG)

  return
}

run()
